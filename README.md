Heatmapper-Docker
=================

Run a Heatmapper-based app in Docker containers.

Links:

* [Heatmapper website](http://www.heatmapper.ca)
* [Heatmapper GitHub repository](https://github.com/WishartLab/heatmapper)


## Server setup ##

1. Set up a server instance and install Docker. If you choose to use Docker Machine, an
example command for AWS is

```
    docker-machine create --driver amazonec2 --amazonec2-region us-east-1
    --amazonec2-ssh-keypath ~/.ssh/id_rsa --amazonec2-instance-type c5.large
    --amazonec2-root-size 30 --amazonec2-ami ami-07ebfd5b3428b6f4d aws-heatmapper-1
```

2. Install Docker Compose. For example, according to instructions [here](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04):

```
    sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
```

3. In a user account with admin privileges, clone `heatmapper-docker`, then clone the
[Heatmapper repository](https://github.com/WishartLab/heatmapper) and put it within the
`web` directory:

```
    cd heatmapper-docker
    git clone https://github.com/WishartLab/heatmapper.git web/heatmapper
```

4. Create a file `web/config/config.R` (in the same format
as `web/heatmapper/config.R`). To have the app's custom logs written to the `web/log`
directory on the host, add `logDir = '/log'` to `config.R`. The config file should look
like:

```
    # Logging
    logDir = '/log' # App's custom logs will be written to web/log on the host.
```

* In summary, the file structure should look like:
```
    heatmapper-docker
      web
        config
          config.R
        heatmapper
        log
```

5. Build and run:

```
    cd heatmapper-docker
    sudo docker-compose build
    sudo docker-compose rm -sf
    sudo docker-compose up -d
```

* Note: It can take 50 minutes to build the containers.

* Once the containers are running, the website should be accessible on port 80.

6. On the host, the following cron jobs should be installed in the `root` user's crontab
to keep the containers running and prevent disk space from being exhausted:

```
    # Restart Heatmapper containers periodically.
    */5  *  *  *  *  /usr/local/bin/docker-compose -f /home/ubuntu/heatmapper-docker/docker-compose.yml up -d

    # Periodically free up some disk space that Docker accumulates.
    0 13 * * 6 /usr/bin/docker system prune -f
```


## Organization ##

1. `nginx.conf` is for NGINX balancer container's config.
2. `web` is for the web server source code and Dockerfile.
3. `web/heatmapper` should be a copy of the
   [Heatmapper repository](https://github.com/WishartLab/heatmapper).
4. `web/config/config.R` should be created, in the
   same format as `web/heatmapper/config.R`.
5. `web/log` will contain log files written by Heatmapper.
6. `web/cron_root` is for crontab jobs in web containers.
7. Inside the web containers, Shiny Server writes job files to `/tmp`. cron jobs will
   copy contents of `/tmp` to `/JOBS` and delete files older than 7 days. (Problems arise
   if trying to map a host directory directly to the containers' `/tmp`).
8. If you decide to set up a native Apache web server instead of using the NGINX container
   (see below), `heatmapper.conf` can be moved to `/etc/httpd/conf.d/` to boot up the
   server with host domain.


## Optional: Install web server natively on host on port 80 ##

```
sudo apt install apache2
sudo a2enmod remoteip
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_balancer
sudo a2enmod lbmethod_byrequests
sudo cp ~/heatmapper-docker/heatmapper.conf /etc/apache2/sites-available/.
sudo systemctl nginx stop
sudo systemctl apache2 start
```
