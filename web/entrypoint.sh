#!/bin/bash
set -e


cd /root/.
. .bash_profile
crond start
echo `pwd`
echo "Executing" "$@"

exec "$@"
